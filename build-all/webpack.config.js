/*
 Not much to see here. This is a utility config to allow building everything in a single go.

 Generally all requires are best declared immediately in a module (.js file).
 If not at least make sure they NEVER get declared in functions:
 See: http://justbuildsomething.com/node-js-best-practices/#2
 */

const productionReadyBuilds = [].concat(
    // This one-liner is so simple it doesn't need any sort of special care
    require('../build-1-minimal/webpack.config'),
    // Babel turns ES6 back to ES5 to allow UglifyJs to minify it
    require('../build-7-babel/webpack.config')
);
const allBuilds = productionReadyBuilds.concat(
    /*
     These builds bundle ES6 code that the default UglifyJs is unable to deal with.
     See: https://www.npmjs.com/package/uglify-js
     */
    require('../build-2-npm-packages/webpack.config'),
    require('../build-3-analyze/webpack.config'),
    require('../build-4-loaders/webpack.config'),
    require('../build-5-webpage/webpack.config'),
    require('../build-6-styles/webpack.config')
);

module.exports = process.env.NODE_ENV === 'production' ? productionReadyBuilds : allBuilds;

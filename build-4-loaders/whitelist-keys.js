/*
 Returns a predicate that only allows property file lines
 that are included in the whitelisted keys
*/
module.exports = function whitelistKeys(keys) {
  const isComment = line => line.trim().startsWith('#');
  const isParseable = line => line.includes('=');
  const isIncluded = line => keys.includes(keyOf(line));
  return line => !isComment(line) && isParseable(line) && isIncluded(line);
};

function keyOf(line) {
  return line.split('=')[0].trim();
}

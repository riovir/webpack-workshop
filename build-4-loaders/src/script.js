// Webpack can resolve installed node modules
import ie from 'ws-translations/dist/resources_ie.properties';
import base from 'ws-translations/dist/resources.properties';
// And local files the same way
import en from './translation-en.yaml';
import hu from './translation-hu.yaml';

console.log('English', en);
console.log('Hungarian', hu);
console.log('Deluxe', Object.assign({}, base, ie));

const path = require('path');
// const whitelist = require('./whitelist-keys');

const exampleName = '4-loaders';

/*
 Note that the yaml-loader (https://github.com/okonet/yaml-loader)
 does not return a parsed object. Instead it returns a JSON string.
 Since we want an actual object, we can feed it into the json-loader
 (comes out of box with Webpack).
 Additional loaders: https://github.com/webpack/docs/wiki/list-of-loaders
*/
module.exports = {
  entry: {
    bundle: `./build-${exampleName}/src/script.js`
  },
  output: {
    path: path.resolve(__dirname, `../dist/${exampleName}`),
    filename: '[name].js'
  },
  module: {
    rules: [
      /* Imline style */
      /*
       The ! denotes a 'pipe' to separate loaders.
       They are read from right to left.
       */
      { test: /\.yaml$/, loader: 'json-loader!yaml-loader' },

      /* Verbose style */
      // { test: /\.yaml$/, use: [{ loader: 'json-loader' }, { loader: 'yaml-loader' }] },

      /* Inline style is enough most of the time */
      { test: /\.properties$/, loader: 'properties-loader!ws-filter-loader?include=^todo-management' }

      /*
       But the verbose style gives more control,
       eg. the ability to pass functions as options
       */
      // { test: /\.properties$/,
      //   use: [
      //     { loader: 'properties-loader' },
      //     { loader: 'ws-filter-loader',
      //       options: {
      //         include: whitelist(['todo-management.edit-view.description', 'todo-management.edit-view.header'])
      //       } }
      //   ] }
    ]
  }
};

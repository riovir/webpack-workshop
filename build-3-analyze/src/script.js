import { cloneDeep } from 'lodash';
// import cloneDeep from 'lodash.clonedeep';
// import cloneDeep from '../off-topic/caveman-copy';

const master = {
  name: 'Roel',
  skills: ['Java', 'Lombok']
};

/*
 Note: Object.assign is NOT meant to be used as a means to create clones.
 All non-primitive props like an array will be assigned as reference,
 */
// const padavan = Object.assign({}, master);

const padavan = cloneDeep(master);
padavan.name = 'Ginneken';
padavan.skills.push('ES6');

logSkills(master);
logSkills(padavan);

function logSkills(person) {
  console.log(`${person.name} knows ${person.skills.join(', ')}`);
}

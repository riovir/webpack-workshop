const path = require('path');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

const exampleName = '3-analyze';

module.exports = {
  entry: {
    bundle: `./build-${exampleName}/src/script.js`
  },
  output: {
    path: path.resolve(__dirname, `../dist/${exampleName}`),
    filename: '[name].js'
  },
  plugins: [
    // See config at: https://www.npmjs.com/package/webpack-bundle-analyzer
    new BundleAnalyzerPlugin({
      // Creates a static report HTML to visualize the bundle
      analyzerMode: 'static',
      // For in depth look turn on the strats.json and open it here: https://webpack.github.io/analyse/
      generateStatsFile: true,
      // Disable default behavior of opening a new browser tab on every build
      openAnalyzer: false
    })
  ]
};

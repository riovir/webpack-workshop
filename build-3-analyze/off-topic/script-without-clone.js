/*
Although this excersise is about bundling 3rd party packages, such as lodash
Creating copies of object tends to come up quite often in ES6.
Easiest way to address the topic is wrapping objects in factory functions which know
how to make a fresh instance when needed.
*/

/* Option A: With minimal changes to the original code */
const master = Master();
const padavan = Master();
padavan.name = 'Ginneken';
padavan.skills.push('ES6');

function Master() {
  return { name: 'Roel', skills: ['Java', 'Lombok'] };
}

/* Option B: This style is more flexible but needs more ES6 knowledge to understand */
// const master = Person({
//   name: 'Roel',
//   skills: ['Java', 'Lombok']
// });
// const padavan = Person({
//   name: 'Ginneken',
//   skills: master.skills.concat('ES6')
// });
//
// function Person({ name = null, skills = [] } = {}) {
//   return { name, skills: [].concat(skills) };
// }

/* Option C: You tell me. :) There is always another option! */

logSkills(master);
logSkills(padavan);

function logSkills(person) {
  console.log(`${person.name} knows ${person.skills.join(', ')}`);
}

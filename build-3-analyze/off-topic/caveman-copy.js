/*
 This is safe to use if you know that the object is just a dumb data structure.
 Cloning an object becomes less of an issue if your project avoids
 - using classes
 - relying on protypes, this, instanceof
 - ES6 getters and setters

 It's also advisable to separate objects that exclusively hold data
 (called as data structures) and objects (ideally just simple functions)
 that work on the data structures. This way you'll end up with a code base
 where data easy to serialize, and behavior is separated from the data.
*/
export default function cavemanCopy(object) {
  console.log('You do know that this will wipe away all fancy stuff like getters, setters, prototypes and functions, right?');
  return JSON.parse(JSON.stringify(object));
}

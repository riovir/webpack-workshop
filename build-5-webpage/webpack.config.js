const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const exampleName = '5-webpage';

module.exports = {
  entry: {
    bundle: `./build-${exampleName}/src/main.js`
  },
  output: {
    path: path.resolve(__dirname, `../dist/${exampleName}`),
    publicPath: `/${exampleName}`,
    filename: '[name].js'
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './src/page-template.html'),
      filename: 'welcome.html' // Optional, defaults to index.html
    })
  ]
};

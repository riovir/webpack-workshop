console.log(`Let's see how IE handles some ES6`);

// Some facy way to assign array elements to wariables.
const colors = ['red', 'green', 'blue', 'meh'];
const [first, second, third] = colors;

console.log(`IE doesn't like ${first}`);
console.log(`IE doesn't like ${second}`);
console.log(`IE doesn't like ${third}`);

# webpack-workshop

## Terminology

* ```Module```: a ```.js``` file that delivers value via exports. Allows users to import either its default export or any of the optional, named exports. Ideally a module exports a single default functionality.
* ```Script```: a ```.js``` file that delivers value via getting executed. It typically doesn't offer any importable parts.
* ```Bundle```: a generated ```script``` (or in special cases ```module```) that contains the intended contents of an application or library ready to be consumed. The consumer is usually the browser, but also can be Node.js.

## Try it out

```
git clone https://gitlab.com/riovir/webpack-workshop.git
cd webpack-workshop
npm run init
npm run intro
```

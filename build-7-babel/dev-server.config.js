module.exports = {
  compress: true,
  historyApiFallback: true,
  hot: true,
  port: 3000
};

// Teaches IE 11 the global API ES6 expect, such as Object.assign or Promise
import 'babel-polyfill';
import 'ws-colorize';
import $ from 'jquery';

console.log(`Let's see how IE handles some ES6`);

// Some facy way to assign array elements to wariables.
const colors = ['red', 'green', 'blue', 'meh'];
const [first, second, third] = colors;
const printColor = color => { console.log(`IE now likes ${color}`); };

printColor(first);
printColor(second);
printColor(third);

// ES6 global object
const message = { text: 'No clue what Object.assign is' };
const actualMessage = Object.assign(message, { text: 'Object.assign works' });
console.log(actualMessage.text);

// Let's use our self-developed jQuery plugins
$('.colorized').colorize();

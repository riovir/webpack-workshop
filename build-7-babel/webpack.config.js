const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const devServer = require('./dev-server.config');

const exampleName = '7-babel';

module.exports = {
  entry: {
    bundle: `./build-${exampleName}/src/main.js`
  },
  output: {
    path: path.resolve(__dirname, `../dist/${exampleName}`),
    publicPath: `/${exampleName}`,
    filename: '[name].js'
  },
  module: {
    rules: [
      /*
       In order to ensure a clean bundle lint .js sources first (failing the build on error)
       Then feed it to the babel loader which (using a preset configured in the .babelrc)
       will transpile it. Typically this needs to be done due to some customers still refusing
       to use a decent browser.
       IMPORTANT: we are not done yet! For full ES6 support the babel-polyfill needs to be imported in the code
       Failing to do so will leave IE 11 wondering what a Promise or Object.assign is

       Note how node_modules are skipped. They are (expected) to be published in a universally digestable format.
       (For browsers that would be ES5, UMD format, but more on that later)
       Imagine the alternative: do you want to start configuring a TypeScript transpiler here, just because an
       npm package author prefers to code in that language?
       */
      { test: /\.js$/, loader: 'babel-loader!eslint-loader?failOnError=true', exclude: /node_modules/ }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './src/page-template.html'),
      filename: 'welcome.html'
    }),
    new BundleAnalyzerPlugin({
      analyzerMode: 'static',
      generateStatsFile: true,
      openAnalyzer: false
    }),
    new webpack.HotModuleReplacementPlugin()
  ],
  // The babel-loader listens to this general config to ficure out of sourceMaps are needed
  devtool: '#source-map',
  devServer
};

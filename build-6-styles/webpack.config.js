const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const exampleName = '6-styles';

module.exports = {
  entry: {
    bundle: `./build-${exampleName}/src/main.js`
  },
  output: {
    path: path.resolve(__dirname, `../dist/${exampleName}`),
    publicPath: `/${exampleName}`,
    filename: '[name].js'
  },
  module: {
    rules: [
      /*
       From right to left:
         - use the less loader to compile less files to CSS with the sourceMap
           option set to true (otherwise good luck debugging styles)
         - feed the result to the postcss-loader allowing post-processing on the css
           - this loader uses a postcss.config.js
           - we use it to add all the vendow prefixes to the generated css,
             you don't need to pollute your source files with them
           - also update the sourceMap
         - process the resulting CSS with its loader (sourceMaps again!)
         *** At this point the css would end up in the bundle.js. Instead we want it
             separate to split requesting the scripts from styles ***
         - feed all the CSS insto the ExtractTextPlugin, whilch at the right time
           in the bundling process will spill its accumulateed content into a CSS file
           following its configuration
       */
      { test: /\.less$/, loader: ExtractTextPlugin.extract('css-loader?sourceMap!postcss-loader?sourceMap!less-loader?sourceMap') },
      // Same as above, minus the less compilation. You won't need this if you only import less files
      { test: /\.css$/, loader: ExtractTextPlugin.extract('css-loader?sourceMap!postcss-loader?sourceMap') }
    ]
  },
  plugins: [
    // Take the name of the entry point and export styles as CSS
    new ExtractTextPlugin('[name].css'),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './src/page-template.html'),
      filename: 'welcome.html'
    })
  ],
  devtool: '#source-map'
};

import { cloneDeep } from 'lodash';

const master = {
  name: 'Roel',
  skills: ['Java', 'Lombok']
};

const padavan = cloneDeep(master);
padavan.name = 'Ginneken';
padavan.skills.push('ES6');

logSkills(master);
logSkills(padavan);

function logSkills(person) {
  console.log(`${person.name} knows ${person.skills.join(', ')}`);
}

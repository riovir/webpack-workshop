const path = require('path');

const exampleName = '2-npm-packages';

module.exports = {
  entry: {
    /* Entry points can also be named. The name can be referred as [name] in the output.
       This will become more helpful when declaring more than a single entry point */
    'named-bundle': `./build-${exampleName}/src/script.js`
  },
  output: {
    path: path.resolve(__dirname, `../dist/${exampleName}`),
    filename: '[name].js'
  }
};

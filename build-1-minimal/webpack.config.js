const path = require('path');

module.exports = {
  // Set the entrypoint AKA the main
  entry: './build-1-minimal/src/script.js',
  output: {
    // Make sure to set the base output path
    path: path.resolve(__dirname, '../dist/1-minimal'),
    filename: 'bundle.js' // ...and reulting bundle name separately
    /*
     Don't merge the output path with the bundle name!
     None of the loaders and plugins will understand that
     dist' is meant to be the output folder.
     You'd need to keep setting all sorts of unnecessary
     options in your configs to tame the build.
    */
    // filename: './dist/[name].js'
  }
};

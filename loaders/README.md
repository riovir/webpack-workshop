# Disclaimer

This is a sandbox environment for playing with loaders. It's unlikely you ever need to write one, unless you need to load
some company specific file, or your project is on the bleeding edge and the Node / ES community didn't have the time
to implement your use case.

It's also recommended to have packages in their own repo. It's however possible to incubate them in a parent project
until they are ready to be moved out.

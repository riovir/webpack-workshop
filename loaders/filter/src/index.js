const loaderUtils = require('loader-utils');

module.exports = function filterLoader(content) {
  // It's good practice to declate defaults for each option
  const defaults = { include: () => true };
  // Merge the defaults with use provided overrides
  const options = Object.assign({}, defaults, loaderUtils.getOptions(this));

  return content.split('\n')
      .filter(filterWith(options.include))
      .join('\n');
};

function filterWith(include) {
  // If it's a function use it as a predicate
  if (typeof include === 'function') {
    return line => include(line);
  }
  // Strings are assumed to be regular expressions
  if (typeof include === 'string') {
    const regexp = new RegExp(include);
    return line => regexp.test(line);
  }
  // The ES6 regular expression is an object with a test funcion
  if (include.test) {
    return line => include.test(line);
  }
  throw new Error('filter-loader: Include has to be either a regular expression, or predicate function');
}

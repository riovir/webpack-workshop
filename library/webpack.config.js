const path = require('path');
const webpack = require('webpack');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

const libraryName = 'ws-colorize';

module.exports = {
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, './lib'),
    filename: `${libraryName}.js`,
    library: libraryName,
    libraryTarget: 'umd',
    umdNamedDefine: true
  },
  module: {
    rules: [
      { test: /\.js$/, loader: 'babel-loader!eslint-loader?failOnError=true', exclude: /node_modules/ }
    ]
  },
  plugins: [
    new BundleAnalyzerPlugin({
      analyzerMode: 'static',
      generateStatsFile: true,
      openAnalyzer: false
    }),
    /*
     This is normally unnecessary. ...if the source can be bothered to actially
     import jquery if it's required. Unfortunately some jQuery plugins
     (I'm looking at you, Semantic UI behaviors) just assume a $ object is available globally.
     This is a way to deal with that.
     */
    new webpack.ProvidePlugin({ $: 'jquery' })
  ],
  externals: ['jquery'],
  devtool: '#source-map'
};

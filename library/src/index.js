/*
 Intentionally forgetting to import $ and just assuming it's available globally.
 Not a very nice thing to do, but at least I can show you the webpack.ProvidePlugin
 in the webpack.config.js
*/
// import $ from 'jquery';
import randomcolor from 'randomcolor';

$.fn.colorize = function register() { // eslint-disable-line no-undef
  const setRandomColor = (index, element) => $(element).css('color', randomcolor()); // eslint-disable-line no-undef
  this.each(setRandomColor);
  return this;
};
